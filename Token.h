//una clase para cada expresion que se vaya a ingresar
#include<iostream>
#include<string>

using namespace std;

class Token{
    string valor;
    int prioridad;

    public:
        Token(string valor){
            this->valor = valor;
            if(valor == "+" || valor == "-"){
                this->prioridad = 1;
            }else if(valor == "*" || valor == "/"){
                this->prioridad = 2;
            }else if(valor == "(" || valor == ")"){
                this->prioridad = 3;
            }else{
                this->prioridad = 0;
            }
        }
        Token(){

        }
        string getValor(){
            return valor;
        }
        void setValor(string valor){
            this->valor = valor;
        }
        int getPrioridad(){
            return prioridad;
        }
        void setPrioridad(int prioridad){
            this->prioridad = prioridad;
        }
};