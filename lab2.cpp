#include<iostream>
#include"stack.h"

using namespace std;

NodoToken *operar(float num1, float num2, string operador){
    float resultado;
    if (operador == "+"){
        resultado = num1 + num2;
    }else if (operador == "-"){
        resultado = num1 - num2;
    }else if (operador == "*"){
        resultado = num1 * num2;
    }else{
        resultado = num1 / num2;
    }
    Token token_resultado(to_string(resultado));
    NodoToken *nodo_resultado = new NodoToken(token_resultado);
    return nodo_resultado;
}
string strip_parent(string input){
    // si el en primer indice y en el ultimo indice hay un ( y un )
    // respectivamente, eliminarlos.
    char primero;
    primero = input[0];
    char ultimo;
    ultimo = input[input.length()-1];
    if (primero == '(' && ultimo == ')'){
        input.erase(input.length()-1, 2);
        input.erase(0, 2);
    }
    input.erase(input.length() - 1, 2);
    return input;
}

string buscar(string str, int x){
    string pedazo = "";
    int parent = 0;
    for (int i=x; i<str.length(); i++){
        pedazo += str[i];
        if (str[i] == '('){
            parent += 1;
        }else if (str[i] == ')'){
            parent -= 1;
        }
        if (parent == 0){
            break;
        }
    }
    return strip_parent(pedazo);
}

int busca_piso(string str, int x){
    int parent = 0;
    int piso;
    for (int i=x; i<str.length(); i++){
        if (str[i] == '('){
            parent += 1;
        }else if (str[i] == ')'){
            parent -= 1;
        }
        if (parent == 0){
            piso = i;
            break;
        }
    }
    return piso;
}

NodoToken *separar(string texto){
    texto += " ";
    string elemento = "";
    Stack pila_numeros, pila_ops;
    for (int i=0; i<texto.size(); i++){
        if (texto[i] != ' '){
            elemento += texto[i];
        }else{
            Token token(elemento);
            elemento = "";
            NodoToken *nodo = new NodoToken(token);
            if (nodo->getToken().getPrioridad() == 0){
                pila_numeros.push(nodo);
            }else if (nodo->getToken().getPrioridad() < 3){
                if (pila_ops.esVacio()){
                    pila_ops.push(nodo);
                }else{
                    if (nodo->getToken().getPrioridad() <= pila_ops.getSup()->getToken().getPrioridad()){
                        //operar cosas
                        int num1, num2;
                        string operador;
                        num2 = stoi(pila_numeros.pop()->getToken().getValor());
                        num1 = stoi(pila_numeros.pop()->getToken().getValor());
                        operador = pila_ops.pop()->getToken().getValor();
                        NodoToken *resultado = operar(num1, num2, operador);
                        pila_numeros.push(resultado);
                        pila_ops.push(nodo);
                    }else{
                        pila_ops.push(nodo);
                    }
                }
            }else if (nodo->getToken().getValor() == "("){
                //cosas paréntesis
                string buscada = buscar(texto, i - 1);
                i = busca_piso(texto, i - 1) + 1;
                NodoToken *nodo_parentesis = separar(buscada);
                pila_numeros.push(nodo_parentesis);
                if (i == texto.size() - 1){
                    break;
                }
            }
        }
    }
    while (!pila_ops.esVacio()){
        int num1, num2;
        string operador;
        num2 = stoi(pila_numeros.pop()->getToken().getValor());
        num1 = stoi(pila_numeros.pop()->getToken().getValor());
        operador = pila_ops.pop()->getToken().getValor();
        NodoToken *resultado = operar(num1, num2, operador);
        pila_numeros.push(resultado);
    }
    return pila_numeros.getSup();
}


int main(){
    string problema;
    cout << "ingrese su problema: " << endl;
    getline(cin,problema);
    cout << problema << endl;
    NodoToken *resultado = separar(problema);
    cout << resultado->getToken().getValor() << endl;
    return 0;
}
