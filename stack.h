#include "NodoToken.h"

class Stack{
    NodoToken *sup;
    public:
        Stack(){
            sup = NULL;
        }
        NodoToken *getSup(){
            return sup;
        }
        void push(NodoToken *nuevo_sup){
            nuevo_sup->setVecino(sup);
            sup = nuevo_sup;
        }
        NodoToken *pop(){
            NodoToken *aux = getSup();
            sup = sup->getVecino();
            return aux;
        }
        bool esVacio(){
            if (sup == NULL)
                return true;
            return false;
        }
};
