#include"Token.h"

class NodoToken{
    Token valor;
    NodoToken *vecino;
    
    public:
        NodoToken(){
            this->vecino = NULL;
        }

        NodoToken(Token nuevo){
            this->valor = nuevo;
            this->vecino = NULL;
        }
        void setVecino(NodoToken *vec){
            this->vecino = vec;
        }
        NodoToken *getVecino(){
            return(this->vecino);
        }

        Token getToken(){
            return(this->valor);
        }
};